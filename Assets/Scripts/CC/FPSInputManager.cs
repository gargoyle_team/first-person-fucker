﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{

    private PlayerMovement playerController;
    private BallShoot ballShoot;
    private Laser playerLaser;
	private MouseCursor mouseCursor;
    private PlayerMovement playerController2;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private Vector2 inputAxis;
    private Vector2 mouseAxis;


    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        playerLaser = playerController.GetComponent<Laser>();
        ballShoot = playerController.GetComponent<BallShoot>();
        lookRotation = playerController.GetComponent<LookRotation>();
		mouseCursor = new MouseCursor();
		mouseCursor.HideCursor();
    }

    void Update()
    {
        //El movimiento del player
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if(Input.GetButton("Jump")) playerController.StartJump();

        //Rotación de la cámara
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);
		if (Input.GetMouseButtonDown (0)) {
			playerLaser.Shot ();
			mouseCursor.HideCursor();
		}
		else if(Input.GetKeyDown(KeyCode.Escape)){
			mouseCursor.ShowCursor();
		}
		if(Input.GetKeyDown(KeyCode.R)) playerLaser.Reload();
        if (Input.GetMouseButtonDown (1)) ballShoot.Shot();
        if (Input.GetMouseButtonDown (2)) ballShoot.ShotMisil();
    }
}