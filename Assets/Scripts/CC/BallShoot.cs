﻿using UnityEngine;
using System.Collections;
 
public class BallShoot : MonoBehaviour {
   
    public GameObject bullet_prefab;
         public GameObject misil_prefab;
    float bulletImpulse = 20f;
 
   
    // Update is called once per frame
    public void Shot () {
        bulletImpulse = 20f;
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        thebullet.GetComponent<Rigidbody>().AddTorque(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
    }
    public void ShotMisil () {
        bulletImpulse = 1000f;
        GameObject thebullet = (GameObject)Instantiate(misil_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Force);
    }
}